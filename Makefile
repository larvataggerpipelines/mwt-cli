.PHONY: all clean

ifeq ($(OS),Windows_NT)
all: Manifest.toml
else
all: Manifest.toml src/mwt/libmwt
endif

Manifest.toml:
	julia --project=. -e 'using Pkg; Pkg.instantiate()'

src/mwt/libmwt:
	cd src/mwt && $(MAKE)

clean:
	$(RM) Manifest.toml
	cd src/mwt && $(MAKE) clean
