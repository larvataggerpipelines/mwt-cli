# mwt-cli

mwt-cli is a commandline utility to run MWT and Choreography on a video file.

## Installation

The general installation procedure is as follows:
```
git clone https://gitlab.com/larvataggerpipelines/mwt-cli
cd mwt-cli
make
```

Note however that [Julia](https://julialang.org/downloads/), git, make, g++, a jre and opencv with development files are required and to be installed first.

On Windows, the `make` command is usually not available and is of lesser use here.
Windows users who wish not to install `make` can type, as a replacement for the `make` command above:
```
julia --project=. -e 'using Pkg; Pkg.instantiate()'
```

### Windows

To install some of the dependencies, [Chocolatey](https://chocolatey.org/install#individual) can come handy (as administrator):

```
choco install make
choco install openjdk17
```

`java` is required; `make` is not, as previously mentioned.

Unlike other architectures, g++ and opencv are neither required unless you want to build from source.

#### from source (not recommended)

In that latter case, you will also need the following dependencies:
```
choco install mingw
choco install cmake
```
and, in addition to the `make` command run in the *mwt-cli* directory:
```
cd src/mwt
make opencv libmwt
```

### Linux

On Ubuntu22.04 (does not work yet!):
```
sudo apt install git make g++ libopencv-dev openjdk-19-jre-headless
```

On openSUSE Tumbleweed:
```
sudo zypper install git make gcc-c++ opencv-devel java-21-openjdk
```

### macOS (ARM architecture)

XCode and [Homebrew](https://brew.sh/) recommended.

```
brew install make pkg-config opencv oracle-jdk
```

## Usage

The commands below are to be run from the *mwt-cli* root directory.

See `julia --project=. src/mwt-cli.jl --help`.

Example:
```
julia --project=. src/mwt-cli.jl ~/Downloads/A1_2022-07-12-115823-0000_ffv1.avi ./data --frame-rate 30 --pixel-thresholds 234 246 --size-thresholds 30 50 --pixel-size 0.073 --date-time 20220712_115823
```

With all the default values, the above command is equivalent to the following exhaustive version:
```
julia --project=. src/mwt-cli.jl ~/Downloads/A1_2022-07-12-115823-0000_ffv1.avi ./data --frame-rate 30 --pixel-thresholds 234 246 --ref-pixel-thresholds 1 180 --size-thresholds 30 50 1000 1500 --dancer-border-size 10 --persistence-threshold 30 --adaptation-rate 1 --background light --pixel-size 0.073 --minimum-time 5 --speed-window 0.1 --date-time 20220712_115823
```

The `mwt-cli.jl` script operates in 3 stages:
* The input video is first processed with the [libmwt](src/mwt/README.md) library; a *data/20220712_115823* directory is created with a *.blobs* and *.summary* files in there; if argument `--date-time` is not provided, the subdirectory is named after the current date and time.
* If argument `--pixel-size` is provided, Choreography processes the *.blobs* and *.summary* files and generates a series of *.spine* and *.outline* files in the same directory, one pair of files per larva.
* Additionally, if argument `--date-time` is provided, the *.spine* and *.outline* files are merged into a single pair of *.spine*-*.outline* files, and the per-larva *.spine*-*.outline* files are deleted.

Most options are passed to `libmwt`/`mwt-core`, whose best element of documentation is probably the included [tests](https://gitlab.com/larvataggerpipelines/mwt-core/-/blob/master/MWT_Library.cc?ref_type=heads#L1983-2008).

Other options (`--pixel-size`, `--minimum-time` and `--speed-window`) target Choreography. See also [Chore.jar's help](https://gitlab.com/larvataggerpipelines/choreography/-/blob/master/src/main/java/mwt/Choreography.java?ref_type=heads#L2037-2071).
