
using ArgParse

include("MWT.jl")
using .MWT

export main

function main(args=ARGS)
    parser = ArgParseSettings(description="mwt-cli: command-line interface for libmwt")
    @add_arg_table! parser begin
        "--frame-rate"
            help = "Frame rate in frames per second"
            arg_type = Float64
            required = true
        "--pixel-size"
            help = "Pixel size in mm"
            arg_type = Float64
        "--date-time"
            help = "Assay ID in format: yyyymmdd_HHMMSS"
        "--pixel-thresholds"
            help = "List of pixel values; lower and higher thresholds"
            arg_type = Int
            nargs = '+'
            required = true
        "--background"
            arg_type = Symbol
            default = :light
        "--size-thresholds"
            help = "List of blob sizes in pixels; lowest, lower, higher and highest thresholds"
            arg_type = Int
            nargs = '+'
            required = true
        "--adaptation-rate"
            help = "Adaptation rate in frames (1-12)"
            arg_type = Int
        "--ref-pixel-thresholds"
            help = "Reference object's intensity thresholds"
            arg_type = Int
            nargs = '+'
        "--persistence-threshold"
            help = "Object persistence threshold"
            arg_type = Int
        "--dancer-border-size"
            help = "Border around each tracked object"
            arg_type = Int
        "--minimum-time", "-t"
            help = "Minimum duration of a track in seconds"
            arg_type = Float64
        "--speed-window", "-s"
            help = "Time window in seconds for average velocity estimation"
            arg_type = Float64
        "--debug"
            help = "Save the segmentation masks"
            action = :store_true
        "input-path"
            help = "Path to movie file"
            required = true
        "output-path"
            help = "Path to output directory"
            required = true
    end
    parsed_args = parse_args(args, parser)

    inputpath = pop!(parsed_args, "input-path")
    if Sys.iswindows() && startswith(inputpath, "~")
        inputpath = joinpath(homedir(), splitpath(inputpath)[2:end]...)
    end
    if !isfile(inputpath)
        @error "File not found" inputpath
        exit()
    end
    outputpath = pop!(parsed_args, "output-path")
    if !isdir(outputpath)
        mkpath(outputpath)
    end
    framerate = pop!(parsed_args, "frame-rate")
    pixelsize = pop!(parsed_args, "pixel-size")
    datetime = pop!(parsed_args, "date-time")
    if !isnothing(datetime)
        if length(datetime) != 15
            @error "--date-time not in the yyyymmdd_HHMMSS format" datetime
            exit()
        end
        try
            year = parse(Int, datetime[1:4])
            month = parse(Int, datetime[5:6])
            day = parse(Int, datetime[7:8])
            hour = parse(Int, datetime[10:11])
            minute = parse(Int, datetime[12:13])
            second = parse(Int, datetime[14:15])
            datetime = (year, month, day, hour, minute, second)
        catch
            @error "--date-time not in the yyyymmdd_HHMMSS format" datetime
            exit()
        end
    end
    background = pop!(parsed_args, "background")
    if background ∉ (:light, :dark)
        @error "--background can be any of: light, dark" background
        exit()
    end

    asoption(opt) = Symbol(replace(opt, "-"=>""))
    isvalid(val) = !(isnothing(val) || isempty(val))

    kwargs = Dict{Symbol, Any}(
        asoption(opt) => val for (opt, val) in pairs(parsed_args) if isvalid(val)
    )
    kwargs[:darkblob] = background === :light

    mwt_cli(inputpath, outputpath, framerate, pixelsize, datetime; kwargs...)
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end

