module MWT

using Dates
using Libdl
using LazyArtifacts

export mwt_cli

function mwt_cli(inputpath, outputpath, framerate, pixelsize=nothing, datetime=nothing;
    minimumtime=5.0, speedwindow=0.1, # Choreography's options
    kwargs...) # mwt-core's options
    datetime′= mwt_core(inputpath, outputpath, framerate, datetime; kwargs...)
    if !(isnothing(datetime′) || isnothing(pixelsize))
        outputpath = joinpath(outputpath, datetime′)
        choreography(outputpath; pixelsize=pixelsize, t=minimumtime, s=speedwindow)
        if !isnothing(datetime)
            mergefiles(outputpath, datetime′)
        end
    end
end

const projectdir = dirname(Base.active_project())

function mwt_core(inputpath, outputpath, framerate, datetime=nothing;
    pixelthresholds, sizethresholds, refpixelthresholds=(1,180),
    persistencethreshold=30, dancerbordersize=10, adaptationrate=1, darkblob=true,
    debug=false)
    if isnothing(datetime)
        datetime = now()
        year, month, day = yearmonthday(datetime)
        hour, minute = Dates.hour(datetime), Dates.minute(datetime)
        second = Dates.second(datetime)
    else
        year, month, day, hour, minute, second = datetime
    end
    @assert all(>=(0), pixelthresholds)
    if length(pixelthresholds) < 2
        pixelthresholds = [collect(pixelthresholds);
                           zeros(eltype(pixelthresholds), 4 - length(pixelthresholds))]
    end
    @assert all(>=(0), sizethresholds)
    if length(sizethresholds) < 4
        sizethresholds = [collect(sizethresholds);
                          zeros(eltype(sizethresholds), 4 - length(sizethresholds))]
    end
    if length(refpixelthresholds) == 1
        refpixelthresholds = [0, refpixelthresholds]
    end
    darkblob = convert(Int, darkblob)
    if Sys.iswindows()
        libmwt = artifact"libmwt"
        for file in readdir(libmwt)
            dest = joinpath(projectdir, "src", "mwt", file)
            if !isfile(dest)
                cp(joinpath(libmwt, file), dest)
            end
        end
    end
    libmwt = joinpath(projectdir, "src", "mwt", "libmwt")
    Libdl.dlopen(libmwt) do mwt
        mwt_process_movie = Libdl.dlsym(mwt, :mwt_process_movie)
        @ccall $mwt_process_movie(inputpath::Cstring, outputpath::Cstring,
                year::Cint, month::Cint, day::Cint, hour::Cint, minute::Cint,
                second::Cint, framerate::Cfloat,
                refpixelthresholds[1]::Cint, refpixelthresholds[2]::Cint,
                pixelthresholds[1]::Cint, pixelthresholds[2]::Cint,
                sizethresholds[1]::Cint, sizethresholds[2]::Cint,
                sizethresholds[3]::Cint, sizethresholds[4]::Cint,
                persistencethreshold::Cint, dancerbordersize::Cint,
                adaptationrate::Cint, darkblob::Cint, debug::Cuchar)::Cint
    end
    #
    datetime = DateTime(year, month, day, hour, minute, second)
    datetime = Dates.format(datetime, "yyyymmdd_HHMMSS")
    return datetime
end

Chore_options = ["--plugin", "Reoutline::exp", "--plugin", "Respine::0.23::tapered=0.28,1,2", "--plugin", "SpinesForward::rebias", "--minimum-biased", "3mm", "-S", "--nanless"]

function choreography(inputdir, outputdir=nothing; pixelsize, t=5, s=0.1, options=Chore_options)
    Chore = artifact"Chore/Chore.jar"
    if isnothing(outputdir)
        outputdir = inputdir
    end
    run(`java -jar $Chore -t $t -s $s -p $pixelsize $options --plugin Extract::outline::spine --target $outputdir $inputdir`)
end

function mergefiles(path, datetime, spareindividualfiles=false)
    allinputfiles = String[]
    for ext in (".spine", ".outline")
        inputfiles = Dict{String, String}()
        basenames = String[]
        for file in readdir(path)
            if endswith(file, ext)
                parts = split(file, ".")
                larvaid = parts[end-1]
                inputfiles[larvaid] = file
                push!(allinputfiles, file)
                basename′= join(parts[1:end-2], ".")
                push!(basenames, basename′)
            end
        end
        @assert allequal(basenames)
        outputfile = joinpath(path, basenames[1] * ext)
        open(outputfile, "w") do o
            for larvaid in sort!(collect(keys(inputfiles)))
                file = inputfiles[larvaid]
                @debug "Appending file" file larva=larvaid
                inputfile = joinpath(path, file)
                tprev = -1.
                open(inputfile, "r") do i
                    for line in readlines(i)
                        line = replace(line, ","=>".")
                        tcurr = parse(Float64, split(line; limit=2)[1])
                        @assert tprev < tcurr
                        tprev = tcurr
                        write(o, join((datetime, larvaid, line), " ") * "\n")
                    end
                end
            end
        end
    end
    if !spareindividualfiles
        for file in allinputfiles
            rm(joinpath(path, file))
        end
    end
end

end
