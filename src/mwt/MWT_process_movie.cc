/* Copyright (C) 2023 Institut Pasteur, Paris
 * Copyright (C) 2023 European Molecular Biology Laboratory, Heidelberg
 * Copyright (C) 2023 François Laurent
 *
 * This file is part of the libmwt subproject in the mwt-cli project and is
 * distributed under the terms of the GNU Lesser General Public Licence
 * version 2.1 (LGPL 2.1).
 * For details, see file LICENSE, or http://www.gnu.org/licences
 *
 * This file applies the Multi-Worm Tracker (MWT) to a video file in the
 * offline and headless regime. No stimuli are defined; only tracking.
 *
 * This work derives from a script that was written to run on an HPC cluster,
 * or a Linux workstation, to process Lautaro Gandara's data, first in the
 * shape of tiff files, and afterwards adapted to avi files.
 *
 * The larvae are assumed to appear as dark blobs on a white background.
 *
 * The mwt-core library from Rex Kerr "Ichoran" is to be compiled with
 * `make lib MWT_Model.o`, and the present script is to be linked with the
 * resulting .o files and proper compilation flags for OpenCV.
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string>
#include <dirent.h>
#include <cassert>
#include <opencv2/opencv.hpp>
#include "MWT_Image.h"
#include "MWT_Library.h"

#include "MWT_process_movie.h"

using namespace std;

void mwt_assert(TrackerEntry *te, string field, bool expected=true) {
  bool check;
  if (field == "image_info_known") {
    check = te->image_info_known == expected;
  } else if (field == "blob_is_dark") {
    assert(expected==false);
    check = te->performance.blob_is_dark == expected;
  } else if (field == "output_started") {
    check = te->output_started == expected;
  } else if (field == "output_info_known") {
    check = te->output_info_known == expected;
  } else if (field == "objects_found") {
    check = te->objects_found == expected;
  } else {
    throw invalid_argument("unknown field: " + field);
  }
  if (!check) {
    cout << "assertion failed: " << field << " != " << (expected ? "true" : "false") << endl;
    exit(EXIT_FAILURE);
  }
}

void mwt_assert(int ret, string funcname, int expected=1) {
  if (ret != expected) {
    cout << funcname << " failed" << endl;
    exit(EXIT_FAILURE);
  }
}

extern "C"
int mwt_process_movie(char* input_path,
                      char* output_path,
                      int year,
                      int month,
                      int day,
                      int hour,
                      int minute,
                      int second,
                      float frame_rate,
                      int ref_pixel_lower_threshold,
                      int ref_pixel_higher_threshold,
                      int pixel_lower_threshold,
                      int pixel_higher_threshold,
                      int size_lowest_threshold,
                      int size_lower_threshold,
                      int size_higher_threshold,
                      int size_highest_threshold,
                      int persistence_threshold,
                      int dancer_border_size,
                      int adaptation_rate,
                      int blob_is_dark,
                      bool debug)
{
  TrackerLibrary mwt;
  TrackerEntry *te;

  int h1, i, ret;
  float t;
  Image8 arena8;
  string filepath = std::string(input_path);

  char strnum[6]; // debug mode only
  double min, max;

  h1 = mwt.getNewHandle();
  te = mwt.all_trackers[h1];

  if (frame_rate == 0.0)
    frame_rate = 30.0;
  if (ref_pixel_lower_threshold == 0)
    ref_pixel_lower_threshold = 1;
  if (ref_pixel_higher_threshold == 0)
    ref_pixel_higher_threshold = 180;
  if (dancer_border_size == 0)
    dancer_border_size = 10;
  if (persistence_threshold == 0)
    persistence_threshold = 30;
  if (adaptation_rate == 0)
    adaptation_rate = 1;
  if (size_highest_threshold == 0)
    size_highest_threshold = 1500;
  if (size_higher_threshold == 0)
    size_higher_threshold = 1000;

  mwt.setDate(h1, year, month, day, hour, minute, second, false);

  mwt.setCombineBlobs(h1, true);
  // from MWT_Library.cc: Set the border around each tracked object so we can follow it moving
  mwt.setDancerBorderSize(h1, dancer_border_size);
  mwt.setObjectSizeThresholds(h1, size_lowest_threshold, size_lower_threshold, size_higher_threshold, size_highest_threshold); // only the lowest thresholds are useful to tweek
  mwt.setObjectPersistenceThreshold(h1, persistence_threshold); // has no effect on per-frame detection
  mwt.setAdaptationRate(h1, adaptation_rate); // 0 does not work; [1-12]

  mwt.enableOutlining(h1, true);
  mwt.enableSkeletonization(h1, true);

  cv::Mat frame, frame8;
  cv::VideoCapture avi(filepath);
  if (!avi.isOpened()) {
    cout << "error opening video stream from file" << endl;
    exit(EXIT_FAILURE);
  }
  // apply background removal
  avi >> frame;
  if (frame.empty()) {
    cout << "could not read frame" << endl;
    exit(EXIT_FAILURE);
  }
  cout << frame.size << endl;
  if (debug) {
    cv::minMaxIdx(frame, &min, &max);
    cout << "min: " << min << ", max: " << max << endl;
  }
  arena8.size = Point(frame.size[0], frame.size[1]);
  arena8.bounds = Rectangle(Point(0,0), arena8.size-1);
  arena8.bin = 1;
  arena8.pixels = new uint8_t[ arena8.bounds.area() ];
  arena8.owns_pixels = true;
  cv::cvtColor(frame, frame8, cv::COLOR_BGR2GRAY);
  memcpy(arena8.pixels, frame8.data, arena8.bounds.area());

  assert(arena8.pixels != NULL);
  mwt.setImageInfo(h1, 8, arena8.size.x, arena8.size.y);
  mwt_assert(te, "image_info_known");

  /* We do not define reference objects here.
     setRefIntensityThreshold is still required though.
     The values passed have no impact, and scanRefs does nothing.
  */

  // from MWT_Library.cc: Set the intensity above or below which we will fill a reference object
  //ret = mwt.setRefIntensityThreshold(h1, ref_pixel_lower_threshold, ref_pixel_higher_threshold);
  //mwt_assert(ret, "setRefIntensityThreshold");
  te->reference_intensities_known = true; // won't use it anyway

  /* setObjectIntensityThresholds does not seem to work properly
     with white objects on dark background.
     Values should be between 1 and 255 (apparently 0 cannot be included),
     and basically we need to take the full range, especially for the second Range (intensity_of_new).
     A single larva is detected otherwise in the tif files.
     avi files require a ROI to exclude the border of the plate.
  */

  //mwt.setObjectIntensityThresholds(h1, 250, 250);
  te->performance.blob_is_dark = blob_is_dark;
  if (blob_is_dark) {
    te->performance.fill_I = DualRange( Range(pixel_higher_threshold, 255), Range(pixel_lower_threshold, 255) );
  } else {
    te->performance.fill_I = DualRange( Range(1, pixel_lower_threshold), Range(1, pixel_higher_threshold) );
  }
  te->object_intensities_known = true;

  /* .blobs and .summary files will start with `20_`.
     This value serves no purpose and "20" has been used as a default in larva labs for a while.
  */

  //cout << "Preparing output directory" << endl;
  mwt.setOutput(h1, output_path, "20", true, true, false);
  mwt_assert(te, "output_info_known");
  mwt.beginOutput(h1);
  mwt_assert(te, "output_started");

  /* No objects are found.
     scanObjects8 is necessary though, for some flags to turn true.
  */

  //cout << "Scanning objects" << endl;
  ret = mwt.scanObjects8(h1, arena8);
  mwt_assert(te, "objects_found");
  //cout << ret << " objects found" << endl;

  i = 0;

  for (;;) {
    avi >> frame;
    if (frame.empty()) break;
    cv::cvtColor(frame, frame8, cv::COLOR_BGR2GRAY);
    memcpy(arena8.pixels, frame8.data, arena8.bounds.area());

    i += 1;
    t = (float)i / frame_rate;
    ret = mwt.loadImage8(h1, arena8, t);
    mwt_assert(ret, "loadImage8");

    /* On the first tens of frames, the detection is not very good.
        It improves over time, though, and about frame 100 the masks are decent.
    */

    //cout << "Processing image " << i << endl;
    ret = mwt.processImage(h1);
    //cout << ret << " larvae found" << endl;

    //mwt.checkErrors(h1);

    cout << "#objects: " << mwt.reportNumber(h1) << ",  average pixel count: " << mwt.reportObjectPixelCount(h1) << endl;

    if (debug) {
      cout << "Writing results image" << endl;
      mwt.showResults8(h1, arena8);
      sprintf(strnum, "%05d", i);
      arena8.writeTiff((filepath.substr(0, filepath.length() - 4) + "_frame" + strnum + ".tif").c_str());
      //cout << "Writing background image" << endl;
      //te->performance.background->writeTiff((filepath.substr(0, filepath.length() - 4) + "_frame" + strnum + "_bg.tif").c_str());
    }
  }

  //cout << "Writing summary" << endl;
  mwt.complete(h1);

  return 0;
}
