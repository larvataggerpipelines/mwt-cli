#ifndef MWT_PROCESS
#define MWT_PROCESS

extern "C"
int mwt_process_movie(char* input_path,
                      char* output_path,
                      int year,
                      int month,
                      int day,
                      int hour,
                      int minute,
                      int second,
                      float frame_rate,
                      int ref_pixel_lower_threshold,
                      int ref_pixel_higher_threshold,
                      int pixel_lower_threshold,
                      int pixel_higher_threshold,
                      int size_lowest_threshold,
                      int size_lower_threshold,
                      int size_higher_threshold,
                      int size_highest_threshold,
                      int persistence_threshold,
                      int dancer_border_size,
                      int adaptation_rate,
                      int blob_is_dark,
                      bool debug);

#endif
