# LibMWT

This library wraps [mwt-core](https://gitlab.com/larvataggerpipelines/mwt-core) written by Nicholas A. Swierczek and Rex A. Kerr, and exports the `mwt_process_movie` function that uses OpenCV to read *.avi* files and pipe the frames through MWT.

As a derived work from a [script](https://gitlab.com/larvataggerpipelines/pesticides/-/blob/ab7f2b6f588e70c3c043df9f704836e25610a244/src/mwt/main.cc) released under the LGPL, this subproject is also licensed under the LGPL v2.1. Note however only the present subproject is subject to the terms of this license.
